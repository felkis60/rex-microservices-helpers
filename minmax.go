package helpers

import "time"

// Max returns the larger of x or y.
func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func Min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

//MinInt64 - возвращает меньший int64 из двух
func MinInt64(a int64, b int64) int64 {
	if a >= b {
		return b
	}
	return a
}

//MaInt64 - возвращает больший int64 из двух
func MaxInt64(a int64, b int64) int64 {
	if a <= b {
		return b
	}
	return a
}

func MaxDuration(fisrt, second time.Duration) time.Duration {
	if fisrt >= second {
		return fisrt
	}
	return second
}

func MinDuration(fisrt, second time.Duration) time.Duration {
	if fisrt <= second {
		return fisrt
	}
	return second
}
