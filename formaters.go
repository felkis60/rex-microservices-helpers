package helpers

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/nyaruka/phonenumbers"

	types "gitlab.com/felkis60/rex-microservices-helpers/api/rest/types"
)

func CleanPhone(input string) string {
	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		panic(err)
	}

	return reg.ReplaceAllString(input, "")
}

func CleanString(str string) (string, error) {

	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		return "", err
	}
	return reg.ReplaceAllString(str, ""), nil
}

func CutPhone(input string) string {
	if len(input) > 10 {
		return input[len(input)-10:]
	}
	return input
}

func EmailCleanup(email string) string {
	email = strings.ToLower(email)
	email = strings.ReplaceAll(email, " ", "")
	return email
}

// GRPCErrorTrimmer - убирает grpc мусор из текста ошибки и возвращает чистую ошибку
func GRPCErrorTrimmer(err error) error {
	if err != nil {
		stringsSlice := strings.Split(err.Error(), "desc = ")
		if len(stringsSlice) > 1 {
			return errors.New(stringsSlice[1])
		}
	}
	return err
}

func FormatAndGetFullInfoForPhone(phone string) (*types.FullPhoneInfo, error) {

	var phonenum, err = phonenumbers.Parse(phone, "RU")
	if err != nil {
		return nil, err
	}

	if !phonenumbers.IsValidNumber(phonenum) {
		return nil, errors.New("not a valid number")
	}

	var fullPhoneInfo types.FullPhoneInfo
	fullPhoneInfo.CountryCode = strconv.FormatInt(int64(phonenum.GetCountryCode()), 10)
	fullPhoneInfo.AreaAndNumber = strconv.FormatUint(phonenum.GetNationalNumber(), 10)
	fullPhoneInfo.Formated = fullPhoneInfo.CountryCode + "-" + fullPhoneInfo.AreaAndNumber

	return &fullPhoneInfo, nil
}

func MassFormatAndGetFullInfoForPhone(input *types.InputPhoneMassFormat) (*[]string, error) {

	var formatedPhones []string

	for i := range input.Phones {
		var phonenum, err = phonenumbers.Parse(input.Phones[i], "RU")
		if err != nil {
			formatedPhones = append(formatedPhones, "")
			continue
		}

		if !phonenumbers.IsValidNumber(phonenum) {
			formatedPhones = append(formatedPhones, "")
			continue
		}

		var fullPhoneInfo types.FullPhoneInfo
		fullPhoneInfo.CountryCode = strconv.FormatInt(int64(phonenum.GetCountryCode()), 10)
		fullPhoneInfo.AreaAndNumber = strconv.FormatUint(phonenum.GetNationalNumber(), 10)
		fullPhoneInfo.Formated = fullPhoneInfo.CountryCode + "-" + fullPhoneInfo.AreaAndNumber

		formatedPhones = append(formatedPhones, fullPhoneInfo.Formated)

	}

	return &formatedPhones, nil
}

func FormatPhone(phone string) (string, error) {
	data, err := FormatAndGetFullInfoForPhone(phone)
	if err != nil {
		return "", err
	}
	return data.Formated, nil
}

func IncrementMonth(date time.Time) time.Time {

	//TODO: Как кокретно увеличивать месяц

	var tempYear = date.Year()
	var tempMonth = date.Month()
	var tempDay = date.Day()

	if date.Month() >= 12 {
		tempYear++
		tempMonth = 1
	} else {
		tempMonth++
	}
	var daysInMonth = DaysInMonth(date.Month(), date.Year())
	if tempDay > daysInMonth {
		tempDay = daysInMonth
	}

	return time.Date(tempYear, tempMonth, tempDay, date.Hour(), date.Minute(), date.Second(), date.Nanosecond(), date.Location())

}

var timeLayouts = []string{
	"2006-01-02T15:04:05-0700",
	"2006-01-02T15:04:05-07:00",
	"2006-01-02T15:04:05Z0700",
	"2006-01-02T15:04:05Z07:00",
	"2006-01-02T15:04:05Z-0700",
	"2006-01-02T15:04:05Z-07:00",
}

func ParseTime(timeString string) (*time.Time, error) {

	for i := range timeLayouts {
		timeTime, err := time.Parse(timeLayouts[i], timeString)
		if err != nil {
			continue
		}
		return &timeTime, nil
	}
	return nil, errors.New("ERROR: Can't parse time: " + timeString)
}
