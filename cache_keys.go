package helpers

import "strconv"

func AddRexApiGatewayNameToString(key *string) {
	*key = "[rex-api-gateway]" + *key
}

func AddCashflowNameToString(key *string) {
	*key = "[cashflow-service]" + *key
}

func GenerateRedisCacheKayForDrivingData(id int64, profile string) string {
	var cacheKey = "driving_data-" + strconv.FormatInt(id, 10) + "-" + profile
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

// GenerateRedisCacheKeyForAllRansOrder - генерирует ключ из параметров поиска RansomOrder'ов для кеша в Redis
func GenerateRedisCacheKeyForAllRansOrder(mainUID string) string {
	var cacheKey = "ransom-order_search_" + mainUID
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

// GenerateRedisCacheKeyForSearchRentOrder - генерирует ключ из параметров поиска RentOrder'ов для кеша в Redis
func GenerateRedisCacheKeyForSearchRentOrder(mainUID string) string {
	var cacheKey = "rent-order_search_" + mainUID
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

func GenerateRedisCacheKeyForVehicleAnalytics(mainUID string, accountToken string) string {
	var cacheKey = "vehicle_analytics_" + accountToken + "_" + mainUID
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

func GenerateRedisCacheKeyForOperationsAnalytics(mainUID string, accountToken string) string {
	var cacheKey = "operations_analytics_" + accountToken + "_" + mainUID
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

func GenerateRedisCacheKeyForRansomOrderBalanceEveryMonth(mainUID string, accountToken string) string {
	var cacheKey = "ransom_order_balance_months_" + accountToken + "_" + mainUID
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

func GenerateRedisCacheKeyForRentOrderBalance(mainUID string, accountToken string) string {
	var cacheKey = "rent_order_balance_" + accountToken + "_" + mainUID
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

func GenerateRedisCacheKeyForFullAnalytics(accountToken string) string {
	var cacheKey = "full_analytics_" + accountToken
	AddRexApiGatewayNameToString(&cacheKey)
	return cacheKey
}

func GenerateBillBalanceCacheKey(billID int64) string {
	var cacheKey = "bill_balance_" + strconv.FormatInt(billID, 10)
	AddCashflowNameToString(&cacheKey)
	return cacheKey

}
