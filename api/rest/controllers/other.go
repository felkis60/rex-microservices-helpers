package controllers

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/andybalholm/brotli"
	gt "github.com/bas24/googletranslatefree"
	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	m "gitlab.com/felkis60/rex-microservices-helpers"
	types "gitlab.com/felkis60/rex-microservices-helpers/api/rest/types"
)

// @Summary Format phone number to standard
// @Tags Phone
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ body types.InputPhoneFormat true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/phones/format [post]
func FormatPhone(c *gin.Context) {

	var input types.InputPhoneFormat
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	phone, err := m.FormatAndGetFullInfoForPhone(input.Phone)
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, m.RestRespone{Message: "Success!", Payload: phone})
}

// @Summary Format phone numbers to standard
// @Tags Phone
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ body types.InputPhoneMassFormat true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/phones/mass-format [post]
func MassFormatPhone(c *gin.Context) {

	var input types.InputPhoneMassFormat
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	phones, err := m.MassFormatAndGetFullInfoForPhone(&input)
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, m.RestRespone{Message: "Success!", Payload: phones})
}

// @Summary Format image to some size
// @Tags Images
// @Description
// @Accept  json
// @Produce  json
// @Param  height query int true "height"
// @Param  width query int true "width"
// @Param  mode query int true "mode"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/images/format [post]
func ImageFormating(c *gin.Context) {
	var input types.InputImageResize
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	fileUrlResponse, err := http.Get(input.FileUrl)
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	// file, err := c.FormFile("file")
	// if err != nil {
	// 	c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
	// 	return
	// }

	fileNameStrigs := strings.Split(input.FileUrl, ".")
	if len(fileNameStrigs) > 1 {
		input.Format = fileNameStrigs[len(fileNameStrigs)-1]
	}

	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	defer fileUrlResponse.Body.Close()

	//fileBytes, err := ioutil.ReadAll(reader)
	//if err != nil {
	//	c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
	//	return
	//}

	imageBytes, err := m.ImageFormating(&input, fileUrlResponse.Body)
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	imageReader := bytes.NewReader(imageBytes)

	_, err = m.GetSelectelStorage()
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	containers, err := m.GetSelectelContainers()
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	trueString := ""
	for _, container := range containers {
		if container.Name() == input.Container {
			lastPoint := strings.LastIndex(input.FileUrl, ".")
			if lastPoint >= 0 {
				trueString = uuid.Must(uuid.NewV4()).String() + input.FileUrl[lastPoint:]
			} else {
				trueString = uuid.Must(uuid.NewV4()).String()
			}

			if err = container.Upload(imageReader, trueString, fileUrlResponse.Header.Get("Content-Type")); err != nil {
				c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
				return
			}

			break
		}
	}

	c.JSON(200, m.RestRespone{Message: "Success!", Payload: &map[string]interface{}{"link": "https://363427.selcdn.ru/" + input.Container + "/" + trueString}})
	return

	// c.Writer.WriteHeader(http.StatusOK)
	// c.Header("Content-Type", "application/octet-stream")
	// c.Header("Content-Transfer-Encoding", "binary")
	// c.Header("Content-Disposition", "attachment; filename=redacted.jpeg")
	// c.Writer.Write(imageBytes)

	//c.JSON(200, m.RestRespone{Message: "Success!", Payload: imageBytes})

}

func UploadToContainer(c *gin.Context) {
	var input types.InputUploadToContainer
	input.Container = c.PostForm("container")

	log.Printf("time1: %d", time.Now())
	file, header, err := c.Request.FormFile("file")
	defer file.Close()
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	fileNameStrigs := strings.Split(header.Filename, ".")
	if len(fileNameStrigs) > 1 {
		input.Format = fileNameStrigs[len(fileNameStrigs)-1]
	}

	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	imageReader := bytes.NewReader(buf.Bytes())

	log.Printf("time2: %d", time.Now())
	api, err := m.GetSelectelStorage()
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	// containers, err := m.GetSelectelContainers()
	// if err != nil {
	// 	c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
	// 	return
	// }
	// log.Printf("time3: %d", time.Now())

	trueString := uuid.Must(uuid.NewV4()).String() + "." + input.Format
	if err = api.UploadBody(imageReader, input.Container, trueString, header.Header.Get("Content-Type")); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}
	log.Printf("time4: %d", time.Now())

	c.JSON(200, m.RestRespone{Message: "Success!", Payload: &map[string]interface{}{"link": "https://363427.selcdn.ru/" + input.Container + "/" + trueString}})
	return
}

// @Summary Translate text
// @Tags Translate
// @Description
// @Accept  json
// @Produce  json
// @Param  text query string true "text"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string
// @Router /v1/translate [post]
func Translate(c *gin.Context) {
	var input types.InputTranslate
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	result, err := gt.Translate(input.Text, "ru", "en")
	if err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	c.JSON(200, m.RestRespone{Message: "Success!", Payload: &map[string]interface{}{"result": result}})
	return
}

func Compressor(c *gin.Context) {
	var input types.InputCompressor
	if err := c.BindJSON(&input); err != nil {
		c.JSON(400, m.RestRespone{Message: err.Error(), Payload: nil})
		return
	}

	if input.Type == "encode" {
		result := string(compressBrotli([]byte(input.Text)))
		c.JSON(200, m.RestRespone{Message: "Success!", Payload: &map[string]interface{}{"result": result}})
		return
	}
	if input.Type == "decode" {
		result := string(decompressBrotli([]byte(input.Text)))
		c.JSON(200, m.RestRespone{Message: "Success!", Payload: &map[string]interface{}{"result": result}})
		return
	}

	c.JSON(404, m.RestRespone{Message: "Not found!"})
}

func compressBrotli(data []byte) []byte {
	var b bytes.Buffer
	w := brotli.NewWriterLevel(&b, brotli.BestCompression)
	w.Write(data)
	w.Close()
	return b.Bytes()
}

func decompressBrotli(data []byte) []byte {
	b, err := ioutil.ReadAll(brotli.NewReader(bytes.NewReader(data)))
	if err != nil {
		log.Fatal("error decoding br response", err)
	}
	return b
}
