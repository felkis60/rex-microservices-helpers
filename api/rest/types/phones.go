package types

type InputPhoneFormat struct {
	Phone string `json:"phone"`
}
type InputPhoneMassFormat struct {
	Phones []string `json:"phones"`
}

type FullPhoneInfo struct {
	Formated    string `json:"formated"`
	CountryCode string `json:"country_code"`
	//AreaCode      string `json:"area_code"`
	//Number        string `json:"number"`
	AreaAndNumber string `json:"area_and_number"`
}
