package types

type ImageEditingOptions struct {
	Height      *int     `json:"height"`
	Width       *int     `json:"width"`
	AreaHeight  *int     `json:"area_height"`
	AreaWidth   *int     `json:"area_width"`
	Top         *int     `json:"top"`
	Left        *int     `json:"left"`
	Quality     *int     `json:"quality"`
	Compression *int     `json:"compression"`
	Zoom        *int     `json:"zoom"`
	Rotate      *int     `json:"rotate"`
	Threshold   *float64 `json:"threshold"`
	Gamma       *float64 `json:"gamma"`
	Brightness  *float64 `json:"brightness"`
	Contrast    *float64 `json:"contrast"`
}

type InputImageResize struct {
	Width     uint   `json:"width" form:"width"`
	Height    uint   `json:"height" form:"height"`
	Mode      int    `json:"mode" form:"mode"`
	FileUrl   string `json:"file_url" form:"file_url"`
	Container string `json:"container" form:"container"`
	Format    string
}

type InputUploadToContainer struct {
	Container string `json:"container" form:"container"`
	Format    string
}

type InputTranslate struct {
	Text string `json:"text" form:"text"`
}

type InputCompressor struct {
	Text string `json:"text" form:"text"`
	Type string `json:"type" form:"type"`
}
