package main

import (
	"os"

	controller "gitlab.com/felkis60/rex-microservices-helpers/api/rest/controllers"
	startup "gitlab.com/felkis60/rex-microservices-helpers/init"

	"github.com/gin-gonic/gin"

	"github.com/ernado/selectel/storage"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	_ "gitlab.com/felkis60/rex-microservices-helpers/api/rest/server/docs"
)

var SelectelStorage storage.API
var SelectelError error

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("/v1")
	{

		v1.POST("/phones/format", controller.FormatPhone)
		v1.POST("/phones/mass-format", controller.MassFormatPhone)
		v1.POST("/images/format", controller.ImageFormating)
		v1.POST("/containers/upload", controller.UploadToContainer)
		v1.POST("/translate", controller.Translate)
		v1.POST("/compressor", controller.Compressor)

	}
	return r
}

func main() {
	startup.SystemStartup(true, true)
	router := SetupRouter()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	//log.Printf(l.INFStartServer, "rest")
	router.Run(":" + os.Getenv("REST_PORT"))

}
