package helpers

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"net/mail"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"

	docx "github.com/nguyenthenguyen/docx"
)

func ValidEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func GenerateMD5(str string) string {
	hash := md5.Sum([]byte(str))
	md5str := hex.EncodeToString(hash[:])
	return md5str
}

func CalculateAge(birthdate, today time.Time) int {
	today = today.In(birthdate.Location())
	ty, tm, td := today.Date()
	today = time.Date(ty, tm, td, 0, 0, 0, 0, time.UTC)
	by, bm, bd := birthdate.Date()
	birthdate = time.Date(by, bm, bd, 0, 0, 0, 0, time.UTC)
	if today.Before(birthdate) {
		return 0
	}
	age := ty - by
	anniversary := birthdate.AddDate(age, 0, 0)
	if anniversary.After(today) {
		age--
	}
	return age
}

func MakeRequestTest(router *gin.Engine, method string, url string, accountToken string, body interface{}, additionalHeaders *map[string]string) (int, *RestRespone) {
	var resp RestRespone
	w := httptest.NewRecorder()

	var marshaledBody []byte
	var req *http.Request
	var err error
	if body != nil {
		marshaledBody, err = json.Marshal(body)
		if err != nil {
			return 0, nil
		}
		req, err = http.NewRequest(method, url, bytes.NewReader(marshaledBody))
		if err != nil {
			return 0, nil
		}
	} else {
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			return 0, nil
		}
	}
	req.Header.Set("Token", accountToken)
	if additionalHeaders != nil {
		for key, value := range *additionalHeaders {
			req.Header.Set(key, value)
		}
	}
	router.ServeHTTP(w, req)
	err = json.Unmarshal(w.Body.Bytes(), &resp)
	if err != nil {
		return 0, nil
	}

	return w.Code, &resp
}

func FormatDocx(reader *io.ReaderAt, readerSize int64, mapOfFields *map[string]string, data *map[string]string) (*bytes.Buffer, error) {

	doc, err := docx.ReadDocxFromMemory(*reader, readerSize)
	if err != nil {
		return nil, err
	}
	defer doc.Close()
	var edoc = doc.Editable()

	for i := range *mapOfFields {
		var arrayOFStrings = strings.Split((*mapOfFields)[i], "!")
		var fullString = strings.Replace((*mapOfFields)[i], "!", "", -1)
		for j := range arrayOFStrings {
			var cleanStr, err = CleanString(arrayOFStrings[j])
			if err != nil {
				continue
			}

			if strinFromData, ok := (*data)[cleanStr]; ok {
				fullString = strings.Replace(fullString, cleanStr, strinFromData, -1)
			}

		}
		edoc.Replace(i, fullString, -1)
	}

	var writer bytes.Buffer

	if err = edoc.Write(&writer); err != nil {
		return nil, err
	}

	return &writer, nil
}

func GetSizeOfReader(stream io.Reader) int64 {
	var buf bytes.Buffer
	buf.ReadFrom(stream)
	return int64(buf.Len())
}

func CheckSimpleDateString(date string) error {
	if m, err := regexp.MatchString("\\d{4}-\\d{2}-\\d{2}", date); err != nil {
		return err
	} else if !m {
		return errors.New("ERROR: Wrong date format")
	}
	return nil
}

func DaysInMonth(m time.Month, year int) int {
	return time.Date(year, m+1, 0, 0, 0, 0, 0, time.UTC).Day()
}
