package helpers

import (
	crand "crypto/rand"
	"errors"
	"io"
	mrand "math/rand"
	"strings"
	"time"
)

var digitTable = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// Функция для создания случайного x-значного числа
func GetRandNum(digitAmount_ int) (string, error) {

	if digitAmount_ <= 0 {
		return "", errors.New("ERROR: getRandNum: digitAmount_ must be greater than zero")
	}

	b := make([]byte, digitAmount_)
	n, err := io.ReadAtLeast(crand.Reader, b, digitAmount_)
	if n != digitAmount_ {
		return "", err
	}
	for i := 0; i < len(b); i++ {
		b[i] = digitTable[int(b[i])%len(digitTable)]
	}

	return string(b), nil
}

func RandString(n int) string {
	var src = mrand.NewSource(time.Now().UnixNano())
	sb := strings.Builder{}
	sb.Grow(n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return sb.String()
}
