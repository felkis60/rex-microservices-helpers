package helpers

import (
	"os"

	log "github.com/sirupsen/logrus"
)

type serviceFoldFormater struct {
	f log.Formatter
}

func (l serviceFoldFormater) Format(entry *log.Entry) ([]byte, error) {
	entry.Data["service"] = os.Getenv("SERVICE_NAME")
	return l.f.Format(entry)
}

func StartLogs(logger *log.Logger, filePath string) {

	if os.Getenv("LOG_TO_FILE") == "true" {
		file, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		logger.SetOutput(file)
	}

	switch os.Getenv("LOG_LVL") {
	case "info":
		logger.SetLevel(log.InfoLevel)
	case "debug":
		logger.SetLevel(log.DebugLevel)
	case "error":
		logger.SetLevel(log.ErrorLevel)
	case "trace":
		logger.SetLevel(log.TraceLevel)
	case "warn":
		logger.SetLevel(log.WarnLevel)
	}

	logger.SetFormatter(&log.JSONFormatter{})

	logger.SetFormatter(serviceFoldFormater{
		f: logger.Formatter,
	})

}
