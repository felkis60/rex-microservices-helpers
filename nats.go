package helpers

import (
	"github.com/nats-io/nats.go"
)

func createNatsString(accountToken string, serviceName string, command string) string {
	return accountToken + "." + serviceName + "." + command
}

func PushNatsMessage(connection *nats.Conn, accountToken string, serviceName string, command string, body []byte) error {
	if err := connection.Publish(createNatsString(accountToken, serviceName, command), body); err != nil {
		return err
	}
	return nil
}

func PushNatsMessageEncoded(connection *nats.EncodedConn, accountToken string, serviceName string, command string, body interface{}) error {
	if err := connection.Publish(createNatsString(accountToken, serviceName, command), body); err != nil {
		return err
	}
	return nil
}
