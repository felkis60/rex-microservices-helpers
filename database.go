package helpers

import (
	"context"
	"log"
	"math"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
	logrus "github.com/sirupsen/logrus"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// Помощник для пагинации запрашиваемых элементов
func PaginateManual(page_ *int, pageSize_ *int, min int, max int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {

		if *page_ <= 0 {
			*page_ = 1
		}

		switch {
		case *pageSize_ > max:
			if max > 0 {
				*pageSize_ = max
			}
		case *pageSize_ < min:
			*pageSize_ = min
		}

		offset := (*page_ - 1) * *pageSize_
		return db.Offset(offset).Limit(*pageSize_)
	}
}

// Помощник для пагинации запрашиваемых элементов со значениями по умолчанию
func Paginate(page_ *int, pageSize_ *int) func(db *gorm.DB) *gorm.DB {
	return PaginateManual(page_, pageSize_, 5, 100)
}

// Помощник для работы с аккаунтами в БД
func DbAccountLeftJoin(baseModel, accountToken string, db *gorm.DB) *gorm.DB {
	return db.Joins("LEFT JOIN accounts ON "+baseModel+".accounts_id = accounts.id").Where("accounts.token = ?", accountToken)
}

func DbDateTimeIntersectTwo(checkStart, checkEnd time.Time, db *gorm.DB, startField, endField string) *gorm.DB {
	if !checkEnd.IsZero() {
		db = db.Where(startField+" <= ?", checkEnd)
	}
	if !checkStart.IsZero() {
		db = db.Where(endField+" >= ?", checkStart)
	}
	return db
}

// Помощник для нахождения элементов по дате/времени начала и дате/времени окончания
func DbDateTimeIntersect(checkStart, checkEnd *time.Time, db *gorm.DB, checkingField string) *gorm.DB {
	if checkEnd != nil && !checkEnd.IsZero() {
		db = db.Where(checkingField+" <= ?", checkEnd)
	}
	if checkStart != nil && !checkStart.IsZero() {
		db = db.Where(checkingField+" >= ?", checkStart)
	}
	return db
}

// Помощник для нахождения элементов по дате начала и дате окончания
func DbDaysIntersect(checkStart, checkEnd int, db *gorm.DB, startField, endField string) *gorm.DB {
	return db.Where(startField+" <= ? AND "+endField+" >= ?", checkEnd, checkStart)
}

func TimeIntersection(checkStart, checkEnd, existingStart, existingEnd time.Time) bool {
	if checkStart.IsZero() {
		return !checkEnd.Before(existingStart)
	}

	if checkEnd.IsZero() {
		return !checkStart.After(existingEnd)
	}

	return (!checkStart.After(existingEnd) && !checkEnd.Before(existingStart))
}

func DbSetOrder(table, by, dir string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		var tempOrder = ""
		if by != "" {
			if table != "" {
				tempOrder = table + "." + by
			} else {
				tempOrder = by
			}

			switch dir {
			case "", "a", "asc", "ascending":
				tempOrder += " asc"
			case "d", "desc", "descending":
				tempOrder += " desc"

			}

			return db.Order(tempOrder)
		}

		return db
	}
}

func DbGetDataAndPaginateList(orderBy, orderDir string, page, pageSize *int, min, max int, tableName string, data interface{}) func(db *gorm.DB) (*Pagination, error) {
	return func(db *gorm.DB) (*Pagination, error) {

		//Count
		var totalItems int64
		if result := db.Count(&totalItems); result.Error != nil {
			return nil, result.Error
		}

		//Getting data
		if result := PaginateManual(page, pageSize, min, max)(DbSetOrder(tableName, orderBy, orderDir)(db)).Scan(data); result.Error != nil {
			return nil, result.Error
		}

		var totalPages = int(math.Ceil(float64(totalItems) / float64(*pageSize)))

		return &Pagination{Page: *page, Items: &data, TotalItems: totalItems, TotalPages: totalPages}, nil

	}
}

//func DeleteCacheFromRedis(client *redis.Client, context *context.Context, whatChanges string, accountToken string, UID string) error {
//
//	switch whatChanges {
//	case "rent_orders", "rent_order", "rent":
//	case "ransom_orders", "ransom_order", "ransom":
//		client.Del(context, GenerateRedisCacheKeyForAllRansOrder(data.UID, []string{data.User.UID, data.Vehicle.UID}))
//
//	case "operation", "operations", "cashflow_operation", "cashflow_operations":
//	case "bill", "bills", "bills_operation", "bills_operations":
//
//	}
//
//	return errors.New("ERROR: no such case in cache")
//}

func AutoMigrateModelsGORM(db *gorm.DB, models []interface{}) {
	for i := range models {
		err := db.AutoMigrate(&models[i])
		if err != nil {
			logrus.Fatalf("failed to automigrate: %v", err.Error())
		}
	}
}

func FormPostgresDSN() (dsn string) {
	if os.Getenv("DB_HOST") != "" {
		dsn += "host=" + os.Getenv("DB_HOST")
	}
	if os.Getenv("DB_USER") != "" {
		dsn += " user=" + os.Getenv("DB_USER")
	}
	if os.Getenv("DB_PASSWORD") != "" {
		dsn += " password=" + os.Getenv("DB_PASSWORD")
	}
	if os.Getenv("DB_NAME") != "" {
		dsn += " dbname=" + os.Getenv("DB_NAME")
	}
	if os.Getenv("DB_PORT") != "" {
		dsn += " port=" + os.Getenv("DB_PORT")
	}
	if os.Getenv("DB_SSLMODE") != "" {
		dsn += " sslmode=" + os.Getenv("DB_SSLMODE")
	}
	if os.Getenv("DB_TIMEZONE") != "" {
		dsn += " TimeZone=" + os.Getenv("DB_TIMEZONE")
	}
	return dsn
}

func InitPostgres(db *gorm.DB) {

	dsn := FormPostgresDSN()

	newLogger := logger.New(
		log.New(logrus.StandardLogger().Out, "\r\n", log.LstdFlags),
		logger.Config{
			SlowThreshold:             time.Millisecond * 300,
			LogLevel:                  logger.Warn,
			IgnoreRecordNotFoundError: true,
			Colorful:                  false,
		},
	)

	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: newLogger})
	if err != nil {
		logrus.Fatalf("failed to connect to the database: %v", err.Error())
	}
}

func InitRedis(cont context.Context, db *redis.Client) {
	db = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("DB_HOST") + os.Getenv("REDIS_PORT"),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0,
	})

	if err := db.Ping(cont).Err(); err != nil {
		logrus.Fatalf("failed to connect to the redis: %v", err.Error())
	}
}
