package helpers

import (
	"context"
	"os"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	logrus "github.com/sirupsen/logrus"
)

func LoggingAndPanicHandlerGRPC(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (outResp interface{}, outErr error) {

	//time for request duration
	start := time.Now()

	//panic recovery
	defer func() {
		if r := recover(); r != nil {
			logrus.WithFields(logrus.Fields{"method": info.FullMethod, "duration": time.Since(start)}).Warn(r)
			outErr = status.Errorf(codes.Internal, "["+os.Getenv("SERVICE_NAME")+"]: panic: %v", r)
			return
		}
	}()

	//request
	outResp, outErr = handler(ctx, req)

	//logging
	if outErr == nil {
		logrus.WithFields(logrus.Fields{"method": info.FullMethod, "duration": time.Since(start)}).Info()
	} else {
		logrus.WithFields(logrus.Fields{"method": info.FullMethod, "duration": time.Since(start)}).Error(outErr)
	}

	return outResp, outErr
}
