package startup

import (
	"log"

	l "gitlab.com/felkis60/rex-microservices-helpers/log"

	"github.com/joho/godotenv"
)

func SystemStartup(loadEnv_ bool, logs_ bool) {

	if loadEnv_ {
		if !envInited {
			envInited = true
			err := godotenv.Load("configs/.env")
			if err != nil {
				log.Fatal(l.ERREnvLoad)
			}
		}
	}

	if logs_ {
		if !logsInited {
			logsInited = true
			l.StartLogs()
		}
	}

}

var envInited = false
var logsInited = false
var natsInited = false
