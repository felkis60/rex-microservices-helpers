package helpers

import (
	"bytes"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"

	resize "github.com/nfnt/resize"
	types "gitlab.com/felkis60/rex-microservices-helpers/api/rest/types"
)

//func ImageFormating(input *types.ImageEditingOptions, imageBytes []byte) ([]byte, error) {
//
//	var bimgOptiong bimg.Options
//
//	if input.AreaHeight != nil {
//		bimgOptiong.AreaHeight = *input.AreaHeight
//	}
//	if input.AreaWidth != nil {
//		bimgOptiong.AreaWidth = *input.AreaWidth
//	}
//	if input.Brightness != nil {
//		bimgOptiong.Brightness = *input.Brightness
//	}
//	if input.Compression != nil {
//		bimgOptiong.Compression = *input.Compression
//	}
//	if input.Contrast != nil {
//		bimgOptiong.Contrast = *input.Contrast
//	}
//	if input.Gamma != nil {
//		bimgOptiong.Gamma = *input.Gamma
//	}
//	if input.Height != nil {
//		bimgOptiong.Height = *input.Height
//	}
//	if input.Left != nil {
//		bimgOptiong.Left = *input.Left
//	}
//	if input.Quality != nil {
//		bimgOptiong.Quality = *input.Quality
//	}
//	if input.Rotate != nil {
//		bimgOptiong.Rotate = bimg.Angle(*input.Rotate)
//	}
//	if input.Threshold != nil {
//		bimgOptiong.Threshold = *input.Threshold
//	}
//	if input.Top != nil {
//		bimgOptiong.Top = *input.Top
//	}
//	if input.Width != nil {
//		bimgOptiong.Width = *input.Width
//	}
//	if input.Zoom != nil {
//		bimgOptiong.Zoom = *input.Zoom
//	}
//
//	var redactedImage, err = bimg.NewImage(imageBytes).Process(bimgOptiong)
//	if err != nil {
//		return nil, err
//	}
//
//	return redactedImage, nil
//
//}

func ImageFormating(input *types.InputImageResize, imageReader io.Reader) ([]byte, error) {

	var img image.Image
	var err error

	img, _, err = image.Decode(imageReader)
	if err != nil {
		return nil, err
	}

	img = resize.Resize(input.Width, input.Height, img, resize.InterpolationFunction(input.Mode))

	//out, err := os.Create("test_resized.jpg")
	//if err != nil {
	//	return "", err
	//}
	//defer out.Close()
	//jpeg.Encode(out, image, nil)

	buf := new(bytes.Buffer)
	switch input.Format {
	case "gif":
		err = gif.Encode(buf, img, nil)
		if err != nil {
			return nil, err
		}
	case "jpeg", "jpg":
		err = jpeg.Encode(buf, img, nil)
		if err != nil {
			return nil, err
		}
	case "png":
		err = png.Encode(buf, img)
		if err != nil {
			return nil, err
		}
	}
	imageBytes := buf.Bytes()

	return imageBytes, nil

}
