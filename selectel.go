package helpers

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/Kron0S/selectel-1/storage"
)

var SelectelContainers []storage.ContainerAPI
var SelectelStorage storage.API
var SelectelError error

type UploadError string

func (e UploadError) Error() string {
	return "Upload error " + strconv.Quote(string(e))
}

type Form struct {
	Value map[string][]string
}

var (
	authEndpoint string = "https://api.selcdn.ru/v2.0/tokens"
)

func GetSelectelStorage() (storage.API, error) {
	if SelectelStorage != nil {
		log.Print("SelectelStorage STANDALONE")
		return SelectelStorage, nil
	}
	log.Print("SelectelStorage NEW")
	SelectelStorage, SelectelError = storage.New(os.Getenv("SELECTELLOGIN"), os.Getenv("SELECTELKEY"))
	if SelectelError != nil {
		return nil, SelectelError
	}
	return SelectelStorage, nil
}

func GetSelectelContainers() ([]storage.ContainerAPI, error) {
	if len(SelectelContainers) > 0 {
		return SelectelContainers, nil
	}
	SelectelContainers, SelectelError = SelectelStorage.Containers()
	if SelectelError != nil {
		return nil, SelectelError
	}
	return SelectelContainers, nil
}

func Login() {
	var jsonStr = []byte(`{"auth":{"passwordCredentials":{"username":"105539","password":""}}}`)
	req, err := http.NewRequest("POST", authEndpoint, bytes.NewBuffer(jsonStr))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
}

func Upload(b io.Reader, container string, name string) error {
	client := &http.Client{}
	req, err := http.NewRequest("PUT", "https://api.selcdn.ru/v1/SEL_105539/"+container+"/"+name, b)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("X-Auth-Token", "8a3004fc73ee1112c4f006ef4a206280")
	req.Header.Set("Content-Type", "application/octet-stream")
	rsp, _ := client.Do(req)
	if rsp.StatusCode != http.StatusOK {
		log.Printf("Request failed with response code: %d", rsp.StatusCode)
		return UploadError("Request failed with response code")
	}
	return nil
}
