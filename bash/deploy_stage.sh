#!/bin/bash

docker build -f dockerfile.rest -t rex-microservices-helpers_stage . --network=host
docker stop rex-microservices-helpers_stage || true
docker rm rex-microservices-helpers_stage || true
docker run --network host -d -p 6336:6336 -v `pwd`:/srv/app --name rex-microservices-helpers_stage rex-microservices-helpers_stage