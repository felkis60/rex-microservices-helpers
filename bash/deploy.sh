#!/bin/bash

docker build -f dockerfile.rest -t rex-microservices-helpers . --network=host
docker stop rex-microservices-helpers || true
docker rm rex-microservices-helpers || true
docker run --network host -d -p 6326:6326 -v `pwd`:/srv/app --name rex-microservices-helpers rex-microservices-helpers