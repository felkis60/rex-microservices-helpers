package log

import (
	"log"
	"os"
)

func StartLogs() {

	if os.Getenv("LOG_FILE") == "true" {
		file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(file)
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)

}

const (
	ERREnvLoad = "ERROR: Failed to load .env file"
)
