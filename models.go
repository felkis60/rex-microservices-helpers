package helpers

type Pagination struct {
	Page           int                    `json:"page"`
	Items          interface{}            `json:"items"`
	TotalItems     int64                  `json:"total_items"`
	TotalPages     int                    `json:"total_pages"`
	AdditionalData map[string]interface{} `json:"additional_data"`
}

type RestRespone struct {
	Message string      `json:"message"`
	Payload interface{} `json:"payload"`
}

type TokenHeader struct {
	Token string `header:"Token"`
}

type LoginTokenHeader struct {
	AccountToken  string `header:"Account-Token"`
	Authorization string `header:"Authorization"`
}

type AuthTokenHeader struct {
	AccountToken string `header:"Account-Token"`
	AuthToken    string `header:"Auth-Token"`
}

type PeriodCashSummary struct {
	BalanceReplenishment float64 `json:"balance_replenishment"` //Opertaions Category 10
	MonthlyPayments      float64 `json:"monthly_payments"`      //Opertaions Category 8
	Delta                float64 `json:"delta"`                 //Opertaions Category 8 - Opertaions Category 10
	TotalRepairAmount    float64 `json:"total_repair_amount"`   //Opertaions Category 21
}

type OrdersPeriodSummary struct {
	TotalNewAndEndedOrders int64             `json:"total_new_and_ended_orders"`
	RentCashSummary        PeriodCashSummary `json:"retn_cash_summary"`
	RansomCashSummary      PeriodCashSummary `json:"ransom_cash_summary"`
	TotalCashSummary       PeriodCashSummary `json:"total_cash_summary"`
}
