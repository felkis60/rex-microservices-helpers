package helpers

import (
	"sort"
	"strings"
)

func SortUIDs(input *[]string) (*[]string, error) {
	sort.Strings(*input)
	return input, nil
}

func SortAndUniteUIDs(input *[]string) (*string, error) {
	arr, err := SortUIDs(input)
	if err != nil {
		return nil, err
	}

	var tempString = strings.Join(*arr, "_")

	return &tempString, nil
}

func FindInSlice(str string, sls *[]string) bool {
	for i := range *sls {
		if (*sls)[i] == str {
			return true
		}
	}
	return false
}

func FindInt64(value int64, slice []int64) bool {
	for i := range slice {
		if slice[i] == value {
			return true
		}
	}
	return false
}
